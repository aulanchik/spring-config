# Spring Configuration #
### Quick overview ###
* This is sample configuration for Spring Boot using Java Annotation Configuration
* Configurations covered:
	* Spring Data
	* Hibernate
	* Spring Security
	* Spring Mvc
	
## Prerequisites ##	
* Make sure you have installed:
	* Java JDK/JRE 7/8 
	* Gradle ( More info: https://gradle.org/install/ )
	
### How do I get set up? ###
	
	# Windows:
	`
		git clone https://reborn0105@bitbucket.org/reborn0105/spring-config.git
		cd ../spring-config
		gradlew bootRun
	`
	
	# Linux:
	`
		git clone https://reborn0105@bitbucket.org/reborn0105/spring-config.git
		cd ../spring-config && ./gradle bootRun
	`