package io.reborn.springconfig.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("io.reborn.springconfig.repositories")
public class RepositoryConfiguration {
}
