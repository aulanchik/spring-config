package io.reborn.springconfig.configs;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "jpa")
public class JpaConfiguration {

    @Value("${showSql}")                    private Boolean showSql;
    @Value("${showStats}")                  private Boolean showStats;
    @Value("${hbm2auto}")                   private String hbm2auto;
    @Value("${packageToScan}")              private String packageToScan;
    @Value("${database_dialect}")           private String dialect;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private Logger logger;

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() throws SQLException {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabase(Database.valueOf(dataSource.getConnection().getMetaData().getDatabaseProductName()));
        vendorAdapter.setShowSql(showSql);
        vendorAdapter.setGenerateDdl(true);
        return vendorAdapter;
    }

    //additional configuration for hibernate
    private Map<String, Object> additionalProperties() throws SQLException{
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("hibernate.validator.apply_to_ddl", "false");
        properties.put("hibernate.dialect", dialect);
        properties.put("hibernate.generate_statistics", showStats);
        return properties;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() throws SQLException {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(packageToScan);
        em.setJpaVendorAdapter(jpaVendorAdapter());
        em.setJpaPropertyMap(additionalProperties());
        em.afterPropertiesSet();
        return em.getObject();
    }

    @Bean
    public JpaTransactionManager transactionManager() throws SQLException {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory());
        return jpaTransactionManager;
    }
}
