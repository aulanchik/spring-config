package io.reborn.springconfig.configs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

@Configuration
@Import(value  = {
        DataSourceConfiguration.class,
        RepositoryConfiguration.class,
        JpaConfiguration.class,
        ServiceConfiguration.class,
        SecurityConfiguration.class,
        WebMvcConfiguration.class
})
public class MainConfiguration {

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Logger logger(){
        return LoggerFactory.getLogger(this.getClass());
    }
}


