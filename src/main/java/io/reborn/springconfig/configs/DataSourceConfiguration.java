package io.reborn.springconfig.configs;

import ch.qos.logback.core.db.DriverManagerConnectionSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@ConfigurationProperties(prefix = "datasource")
public class DataSourceConfiguration {

    @Value("${driverName}")     private String driverName;
    @Value("${connectUrl}")     private String connectUrl;
    @Value("${username}")       private String username;
    @Value("${password}")       private String password;

    @Bean
    public DataSource dataSource(){
        DriverManagerDataSource datasource = new DriverManagerDataSource();
        datasource.setDriverClassName(driverName);
        datasource.setUrl(connectUrl);
        datasource.setUsername(username);
        datasource.setPassword(password);
        return datasource;
    }
}
